package com.example.luka.mvpexample.network;


import com.example.luka.mvpexample.domain.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by luka on 6.8.2015..
 */
public interface GitHubService {

    @GET("/users?since=150")
    void getUsers(Callback<List<User>> cb);

    @GET("/users/{id}")
    void getUser(@Path("id") int id,Callback<User> cb);

}
