package com.example.luka.mvpexample.di.modul;

import com.example.luka.mvpexample.mvp.interactor.GitUsersListInteractor;
import com.example.luka.mvpexample.mvp.interactor.impl.GitUsersListInteractorImp;
import com.example.luka.mvpexample.mvp.presenter.GitUsersListPresenter;
import com.example.luka.mvpexample.mvp.presenter.impl.GitUsersListPresenterImp;
import com.example.luka.mvpexample.mvp.view.GitUsersListView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by luka on 7.8.2015..
 */

@Module
public class GitUsersListModule {

    private GitUsersListView view;

    public GitUsersListModule(GitUsersListView view) {this.view = view;}

    @Provides
    public GitUsersListView provideView() {return  view;}

    @Provides
    public GitUsersListInteractor provideInteractor (GitUsersListInteractorImp interactor)
    {
        return interactor;
    }

    @Provides
    public GitUsersListPresenter providePresenter(GitUsersListPresenterImp presenter)
    {
        return presenter;
    }




}
