package com.example.luka.mvpexample.mvp.interactor;

import com.example.luka.mvpexample.mvp.listener.GitUserDetailsListener;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUserDetailsInteractor extends BaseInteractor{

    void loadUserDetails(Integer id, GitUserDetailsListener gitUserDetailsListener);
}
