package com.example.luka.mvpexample.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.di.component.DaggerGitUsersListComponent;
import com.example.luka.mvpexample.di.component.GitUsersListComponent;
import com.example.luka.mvpexample.di.modul.GitUsersListModule;
import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.presenter.GitUsersListPresenter;
import com.example.luka.mvpexample.mvp.view.GitUsersListView;
import com.example.luka.mvpexample.ui.adapter.UserAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GitUsersListActivity extends BaseActivity implements GitUsersListView, UserAdapter.UserClickListener  {


    @Bind(R.id.recycler_user_list)
    protected RecyclerView userRecycleList;

    @Inject
    protected GitUsersListPresenter gitUsersListPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_git_users_list);
        ButterKnife.bind(this);

        userRecycleList.setHasFixedSize(true);
        userRecycleList.setLayoutManager(new LinearLayoutManager((this)));

        GitUsersListComponent component = DaggerGitUsersListComponent.builder()
                .gitUsersListModule(new GitUsersListModule(this))
                .build();
        component.inject(this);

        gitUsersListPresenter.loadGitUserList();

    }

    @Override
    public void showUsers(List<User> users) {
        UserAdapter userAdapter = new UserAdapter(users);
        userRecycleList.setAdapter(userAdapter);
    }

    @Override
    public void showUserDetail(User user) {
        //Pokrenuti novi intent
        Intent intent = new Intent(this, GitUserDetailsActivity.class);
        intent.putExtra("UserInformation", user);
        startActivity(intent);
    }

    @Override
    public void onUserClicked(User user) {
        gitUsersListPresenter.onUserSelected(user);
    }
}
