package com.example.luka.mvpexample.mvp.view;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUserDetailsView extends BaseView {

    void showUsername(String username);

    void showId(Integer id);

    void showUrl(String url);

    void showType(String type);

    void showSiteAdmin(Boolean siteAdmin);

}
