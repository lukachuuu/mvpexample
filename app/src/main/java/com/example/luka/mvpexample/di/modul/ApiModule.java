package com.example.luka.mvpexample.di.modul;

import android.util.Log;

import com.example.luka.mvpexample.network.GitHubService;

import dagger.Module;
import dagger.Provides;
import retrofit.Endpoint;
import retrofit.RestAdapter;

/**
 * Created by luka on 6.8.2015..
 */
@Module
public class ApiModule {

    public GitHubService gitHubService;

    @Provides
    public GitHubService provideService(Endpoint endpoint) {
        if (gitHubService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(endpoint)
                    .setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String message) {
                            Log.d("REST-ADAPTER", message);
                        }
                    })
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            gitHubService = restAdapter.create(GitHubService.class);
        }
        return gitHubService;
    }
}
