package com.example.luka.mvpexample.mvp.listener;

import com.example.luka.mvpexample.domain.model.User;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUserDetailsListener extends BaseListener {

    void onSuccess(User user);
}
