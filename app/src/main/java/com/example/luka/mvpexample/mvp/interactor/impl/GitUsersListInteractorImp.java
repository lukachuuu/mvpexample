package com.example.luka.mvpexample.mvp.interactor.impl;

import com.example.luka.mvpexample.domain.model.User;

import com.example.luka.mvpexample.mvp.interactor.GitUsersListInteractor;
import com.example.luka.mvpexample.mvp.listener.GitUsersListListener;
import com.example.luka.mvpexample.network.GitHubService;

import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by luka on 7.8.2015..
 */
public class GitUsersListInteractorImp implements GitUsersListInteractor, Callback<List<User>>{

    private GitUsersListListener gitUsersListListener;

    private boolean isCancled;

    private GitHubService gitHubService;

    /**
     * InteractorImp trazi ovisnost o github servisu
     *
     * @param gitHubService
     */

    @Inject
    public GitUsersListInteractorImp(GitHubService gitHubService)
    {
        this.gitHubService = gitHubService;
    }


    @Override
    public void success(List<User> users, Response response) {
        if(!isCancled) gitUsersListListener.onSucces(users);
    }

    @Override
    public void failure(RetrofitError error) {
        if(!isCancled) gitUsersListListener.onFailure(error.getMessage());
    }

    @Override
    public void loadUserList(GitUsersListListener gitUsersListListener) {
        reset();
        this.gitUsersListListener = gitUsersListListener;
        this.gitHubService.getUsers(this);
    }

    @Override
    public void cancel() {
        isCancled = true;
    }

    @Override
    public void reset() {
        this.isCancled = false;
    }
}
