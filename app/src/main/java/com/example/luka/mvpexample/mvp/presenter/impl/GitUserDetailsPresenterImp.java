package com.example.luka.mvpexample.mvp.presenter.impl;

import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.interactor.GitUserDetailsInteractor;
import com.example.luka.mvpexample.mvp.listener.GitUserDetailsListener;
import com.example.luka.mvpexample.mvp.presenter.GitUserDetailsPresenter;
import com.example.luka.mvpexample.mvp.view.GitUserDetailsView;

import javax.inject.Inject;

/**
 * Created by luka on 7.8.2015..
 */

//Prezenter direktno komunicira s view

public class GitUserDetailsPresenterImp implements GitUserDetailsPresenter, GitUserDetailsListener {

    private final GitUserDetailsView gitUserDetailsView;

    private final GitUserDetailsInteractor gitUserDetailsInteractor;

    @Inject
    public GitUserDetailsPresenterImp(GitUserDetailsView gitUserDetailsView, GitUserDetailsInteractor gitUserDetailsInteractor)
    {
        this.gitUserDetailsView = gitUserDetailsView;
        this.gitUserDetailsInteractor = gitUserDetailsInteractor;
    }

    @Override
    public void loadDetails(User user) {
        gitUserDetailsView.showProgress();
        gitUserDetailsInteractor.loadUserDetails(user.getId(),this);
    }

    @Override
    public void cancel() {
        gitUserDetailsView.hideProgress();
        gitUserDetailsInteractor.cancel();
    }

    @Override
    public void onSuccess(User user) {

    }

    @Override
    public void onFailure(String message) {

    }
}
