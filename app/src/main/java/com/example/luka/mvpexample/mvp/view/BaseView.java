package com.example.luka.mvpexample.mvp.view;

public interface BaseView {

    void showProgress();

    void hideProgress();

    void showError(String message);
}
