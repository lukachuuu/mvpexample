package com.example.luka.mvpexample.mvp.listener;

import com.example.luka.mvpexample.domain.model.User;


import java.util.List;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUsersListListener extends BaseListener{

    void onSucces(List<User> users);

}
