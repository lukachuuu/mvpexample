package com.example.luka.mvpexample.mvp.presenter;

import com.example.luka.mvpexample.domain.model.User;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUserDetailsPresenter extends BasePresenter {

    void loadDetails(User user);
}
