package com.example.luka.mvpexample.mvp.listener;

public interface BaseListener {

    void onFailure(String message);
}
