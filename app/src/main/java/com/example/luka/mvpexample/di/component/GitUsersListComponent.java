package com.example.luka.mvpexample.di.component;

import com.example.luka.mvpexample.di.modul.GitUsersListModule;
import com.example.luka.mvpexample.di.modul.NetworkModule;
import com.example.luka.mvpexample.ui.activity.GitUsersListActivity;


import dagger.Component;

/**
 * Created by luka on 7.8.2015..
 */

@Component(modules = {
        NetworkModule.class,
        GitUsersListModule.class
})
public interface GitUsersListComponent {

    void inject(GitUsersListActivity activity);
}