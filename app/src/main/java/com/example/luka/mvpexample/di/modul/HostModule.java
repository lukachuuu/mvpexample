package com.example.luka.mvpexample.di.modul;

import dagger.Module;
import dagger.Provides;
import retrofit.Endpoint;
import retrofit.Endpoints;

/**
 * Created by luka on 6.8.2015..
 */
@Module
public class HostModule {

        public static final String API_URL = "https://api.github.com";

        @Provides
        public Endpoint provideEndpoint() {
            return Endpoints.newFixedEndpoint(API_URL);
        }
}


