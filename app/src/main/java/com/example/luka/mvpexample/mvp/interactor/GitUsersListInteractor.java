package com.example.luka.mvpexample.mvp.interactor;

import com.example.luka.mvpexample.mvp.listener.GitUsersListListener;

/**
 * Created by luka on 7.8.2015..
 */
public interface GitUsersListInteractor extends BaseInteractor{

    void loadUserList(GitUsersListListener gitUsersListListener);
}
