package com.example.luka.mvpexample.ui.adapter;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luka.mvpexample.R;
import com.example.luka.mvpexample.domain.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by luka on 7.8.2015..
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<User> userList;

    public UserAdapter(List<User> userList)
    {
        this.userList = userList;
    }

    private UserClickListener userClickListener;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user, parent, false);
        return new ViewHolder(v);
    }

    /**
     *
     * Ovdje stavljamo stvari za prikaz
     *
     * @param holder
     * @param position
     */

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final User user = userList.get(position);
        holder.userId.setText(user.getId().toString());
        holder.username.setText(user.getLogin().toString());

        holder.url.setText(user.getUrl().toString());

        Context context = holder.avatar.getContext();

        Picasso.with(context).load(user.getAvatarUrl()).into(holder.avatar);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userClickListener.onUserClicked(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.userList.size();
    }


    /**
     * Bindamo stvari u ViewHolder
     */

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View rootView;

        @Bind(R.id.user_id)
        TextView userId;

        @Bind(R.id.username)
        TextView username;

        @Bind(R.id.iv_poster_image)
        ImageView avatar;

        @Bind(R.id.url)
        TextView url;

        public ViewHolder(View itemView)
        {
            super(itemView);
            rootView = itemView;
            ButterKnife.bind(this,itemView);
        }

    }

    public interface UserClickListener {

        public void onUserClicked(User user);
    }

}


