package com.example.luka.mvpexample.mvp.interactor.impl;

import com.example.luka.mvpexample.domain.model.User;
import com.example.luka.mvpexample.mvp.interactor.GitUserDetailsInteractor;
import com.example.luka.mvpexample.mvp.listener.GitUserDetailsListener;
import com.example.luka.mvpexample.network.GitHubService;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by luka on 7.8.2015..
 */
public class GitUserDetailsInteractorImp implements GitUserDetailsInteractor, Callback<User>{

    private GitUserDetailsListener gitUserDetailsListener;

    private boolean isCanceled;

    private GitHubService service;

    @Inject
    public GitUserDetailsInteractorImp(GitHubService service)
    {
        this.service = service;
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public void reset() {
        isCanceled = false;
    }

    @Override
    public void success(User user, Response response) {
        if (!isCanceled) {
            gitUserDetailsListener.onSuccess(user);
        }
    }

    @Override
    public void failure(RetrofitError error) {
        if (!isCanceled) {
            gitUserDetailsListener.onFailure(error.getMessage());
        }
    }

    //servis vraća usera prema id-u

    @Override
    public void loadUserDetails(Integer id, GitUserDetailsListener gitUserDetailsListener) {
        reset();
        this.gitUserDetailsListener = gitUserDetailsListener;
        service.getUser(id, this);

    }
}
