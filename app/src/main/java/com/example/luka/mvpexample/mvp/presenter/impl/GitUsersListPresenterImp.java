package com.example.luka.mvpexample.mvp.presenter.impl;

import com.example.luka.mvpexample.domain.model.User;

import com.example.luka.mvpexample.mvp.interactor.GitUsersListInteractor;
import com.example.luka.mvpexample.mvp.listener.GitUsersListListener;
import com.example.luka.mvpexample.mvp.presenter.GitUsersListPresenter;
import com.example.luka.mvpexample.mvp.view.GitUsersListView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by luka on 7.8.2015..
 */
public class GitUsersListPresenterImp implements GitUsersListPresenter, GitUsersListListener {

    private final GitUsersListView gitUsersListView;

    private final GitUsersListInteractor gitUsersListInteractor;

    @Inject
    public GitUsersListPresenterImp(GitUsersListView gitUsersListView, GitUsersListInteractor gitUsersListInteractor)
    {
        this.gitUsersListInteractor = gitUsersListInteractor;
        this.gitUsersListView = gitUsersListView;
    }

    @Override
    public void onFailure(String message) {
        gitUsersListView.hideProgress();
        gitUsersListView.showError(message.toString() + "FAIL");
    }

    @Override
    public void loadGitUserList() {
        gitUsersListView.showProgress();
        gitUsersListInteractor.loadUserList(this);
    }

    @Override
    public void onUserSelected(User user) {
        gitUsersListView.showUserDetail(user);
    }

    @Override
    public void cancel() {
        gitUsersListView.hideProgress();
        gitUsersListInteractor.cancel();
    }

    @Override
    public void onSucces(List<User> users) {
        gitUsersListView.hideProgress();
        gitUsersListView.showUsers(users);
    }


}
