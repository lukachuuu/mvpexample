package com.example.luka.mvpexample.di.modul;

import dagger.Module;

/**
 * Created by luka on 7.8.2015..
 */
@Module(includes = {
        HostModule.class,
        ApiModule.class
})
public class NetworkModule {

}